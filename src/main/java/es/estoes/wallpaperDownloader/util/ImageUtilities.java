/**
 * Copyright 2016-2021 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.MicrosoftTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import es.estoes.wallpaperDownloader.window.ChooseWallpaperWindow;
import es.estoes.wallpaperDownloader.window.WallpaperManagerWindow;

/**
 * This class gathers all the utilities needed for image management. 
 * It implements a kind of Singleton pattern not pure and only based on static methods. 
 * Actually, there won't be an object of this class ever 
 * @author egarcia
 *
 */
public class ImageUtilities {

	// Constants
	protected static final Logger LOG = Logger.getLogger(ImageUtilities.class);
	public static final String SORTING_BY_DATE = "sort_by_date";
	public static final String SORTING_NO_SORTING = "no_sorting";
	public static final String SORTING_MULTIPLE_DIR = "multiple_directories";


	// Attributes

	// Getters & Setters
	
	// Methods (All the methods are static)

	/**
	 * Constructor
	 */
	private ImageUtilities() {

	}
	
	/**
	 * Get the image icon for the wallpapers.
	 * @param numWallpapers number of wallpapers
	 * @param from first wallpaper to get
	 * @param sort sorting
	 * @param wallpapersType type of the wallpapers to get (favorite, no favorite)
	 * @return
	 */
	public static ImageIcon[] getImageIconWallpapers(int numWallpapers, int from, String sort, String wallpapersType) {
		File[] wallpapers = {};
		List<File> wallpapersList = new ArrayList<File>();
		int to = from + numWallpapers;
		int j = 0;
		
		switch (sort) {
		case SORTING_BY_DATE:
			wallpapers = WDUtilities.getAllWallpapersSortedByDate(wallpapersType);
			break;
		case SORTING_NO_SORTING:
			wallpapersList = WDUtilities.getAllWallpapers(wallpapersType, WDUtilities.DOWNLOADS_DIRECTORY);
			wallpapers = wallpapersList.toArray(wallpapers);
			break;
		case SORTING_MULTIPLE_DIR:
			// Retrieves all the wallpapers stored on different directories set by the user in the changer
			// property
			PreferencesManager prefm = PreferencesManager.getInstance();
			String changerFoldersProperty = prefm.getPreference("application-changer-folder");
			String[] changerFolders = changerFoldersProperty.split(";");
			for (int index = 0; index < changerFolders.length; index ++) {
				String directory = changerFolders[index];
				wallpapersList.addAll(WDUtilities.getAllWallpapers(wallpapersType, directory));
			}
			wallpapers = wallpapersList.toArray(wallpapers);
			break;
		default:
			break;
		}
		
		switch (wallpapersType) {
		case WDUtilities.WD_ALL:
			// Adding total number of favorite wallpapers to ChooseWallpaperWindow if it exists
			ChooseWallpaperWindow.refreshWallpapersTotalNumber(wallpapers.length);				
			break;
		case WDUtilities.WD_FAVORITE_PREFIX:
			// Adding total number of favorite wallpapers to WallpaperManagerWindow if it exists
			WallpaperManagerWindow.refreshFavoriteWallpapersTotalNumber(wallpapers.length);				
			break;
		case WDUtilities.WD_PREFIX:
			if (WallpaperManagerWindow.lblTotalNoFavoriteWallpapers != null) {
				// Adding total number of no favorite wallpapers to WallpaperManagerWindow if it exists only if WallpaperManagerWindow has been
				// instantiated
				WallpaperManagerWindow.refreshNoFavoriteWallpapersTotalNumber(wallpapers.length);				
			}
			// Adding total number of favorite wallpapers to ChooseWallpaperWindow if it exists
			ChooseWallpaperWindow.refreshWallpapersTotalNumber(wallpapers.length);				
			break;
		default:
			break;
		}
		
		int wallpapersLength = wallpapers.length;
		if (to > wallpapersLength) {
			to = wallpapersLength;
		}
		ImageIcon[] wallpaperIcons = new ImageIcon[0];
		if (wallpapers.length > 0) {
			wallpaperIcons = new ImageIcon[numWallpapers];
			for (int i = from; i < to; i++) {
				ImageIcon originalIcon = new ImageIcon(wallpapers[wallpapersLength - (i + 1)].getAbsolutePath());
				Image scaledImg = ImageUtilities.getScaledImage(originalIcon.getImage(), 127, 100);
				ImageIcon resizedIcon = new ImageIcon(scaledImg);
				// Setting a description (absolute path). Doing this. it will be possible to know this path later
				resizedIcon.setDescription(wallpapers[wallpapersLength - (i + 1)].getAbsolutePath());
				wallpaperIcons[j] = resizedIcon;
				j ++;
			}
		}
		return wallpaperIcons;
	}
	
	/**
	 * Scales an image.
	 * @param srcImg original image
	 * @param width width to scale the image
	 * @param height height to scale the image
	 * @return Image scaled image
	 */
	public static Image getScaledImage(Image srcImg, int width, int height){
	    BufferedImage resizedImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2 = resizedImg.createGraphics();

	    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(srcImg, 0, 0, width, height, null);
	    g2.dispose();

	    return resizedImg;
	}
	
    /**
     * Inserting metadata within an image.
     * 
     * @param wallpaperAbsolutePath wallpaper absolute path
     * @param authorUrlProfile author URL profile
     * @param author author's name
     */
    public static void insertMetadata(String wallpaperAbsolutePath, 
    		String authorUrlProfile, 
    		String author) {
    	File srcImageFile = new File(wallpaperAbsolutePath);
    	String wallpaperModifiedAbsolutePath = wallpaperAbsolutePath.substring(0, wallpaperAbsolutePath.lastIndexOf(".")) + 
    			"-modify" + 
    			wallpaperAbsolutePath.substring(wallpaperAbsolutePath.lastIndexOf("."));
    	File dstImageFile = new File(wallpaperModifiedAbsolutePath);    	
    	OutputStream outputStream = null;
    	
        try {
            TiffOutputSet outputSet = null;

            // note that metadata might be null if no metadata is found.
            final ImageMetadata metadata = Imaging.getMetadata(srcImageFile);
            final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
            if (null != jpegMetadata) {
                // note that exif might be null if no Exif metadata is found.
                final TiffImageMetadata exif = jpegMetadata.getExif();

                if (null != exif) {
                    // TiffImageMetadata class is immutable (read-only).
                    // TiffOutputSet class represents the Exif data to write.
                    //
                    // Usually, we want to update existing Exif metadata by
                    // changing
                    // the values of a few fields, or adding a field.
                    // In these cases, it is easiest to use getOutputSet() to
                    // start with a "copy" of the fields read from the image.
                    outputSet = exif.getOutputSet();
                }
            }

            // if file does not contain any exif metadata, we create an empty
            // set of exif metadata. Otherwise, we keep all of the other
            // existing tags.
            if (null == outputSet) {
                outputSet = new TiffOutputSet();
            }

            final TiffOutputDirectory exifDirectory = outputSet.getOrCreateRootDirectory();
            exifDirectory.removeField(ExifTagConstants.EXIF_TAG_SOFTWARE);
            exifDirectory.add(ExifTagConstants.EXIF_TAG_SOFTWARE, authorUrlProfile);

            exifDirectory.removeField(MicrosoftTagConstants.EXIF_TAG_XPAUTHOR);
            exifDirectory.add(MicrosoftTagConstants.EXIF_TAG_XPAUTHOR, author);

            outputStream = new FileOutputStream(dstImageFile);
            outputStream = new BufferedOutputStream(outputStream);

            new ExifRewriter().updateExifMetadataLossless(srcImageFile, outputStream, outputSet);
            
            // Removing modified temporal wallpaper
            FileUtils.forceDelete(srcImageFile);
            FileUtils.moveFile(dstImageFile, srcImageFile);

        } catch (Exception exception) {
			if (LOG.isInfoEnabled()) {
				LOG.error("Error writing metadata in " + wallpaperAbsolutePath + ". Error: " + exception.getMessage());
			}
		} finally {
            IOUtils.closeQuietly(outputStream);
        }
    }
	
}