/**
 * Copyright 2016-2021 Eloy García Almadén <eloy.garcia.pca@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.estoes.wallpaperDownloader.wallpaper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.MicrosoftTagConstants;
import org.apache.log4j.Logger;
import es.estoes.wallpaperDownloader.provider.Provider;
import es.estoes.wallpaperDownloader.util.PropertiesManager;

public class Wallpaper {
	/**
	 * Constants.
	 */

	/**
	 * Logger.
	 * {@value #LOG}
	 */
	private static final Logger LOG = Logger.getLogger(Wallpaper.class);
	
	/**
	 * JPG type.
	 * {@value #WALLPAPER_IMAGE_TYPE_JPG}
	 */
	public static final String WALLPAPER_IMAGE_TYPE_JPG = "jpg";
	/**
	 * JPEG type.
	 * {@value #WALLPAPER_IMAGE_TYPE_JPEG}
	 */
	public static final String WALLPAPER_IMAGE_TYPE_JPEG = "jpeg";
	/**
	 * PNG type.
	 * {@value #WALLPAPER_IMAGE_TYPE_PNG}
	 */
	public static final String WALLPAPER_IMAGE_TYPE_PNG = "png";
	/**
	 * UNKNOWN type.
	 * {@value #WALLPAPER_IMAGE_TYPE_UNKNOWN}
	 */
	public static final String WALLPAPER_IMAGE_TYPE_UNKNOWN = "unknown";

	/**
	 * Not available.
	 * {@value #NOT_AVAILABLE}
	 */
	public static final String NOT_AVAILABLE = "N/A";

	/**
	 * Attributes.
	 */
	/**
	 * Absolute path where the wallpaper is stored.
	 */
	private String absolutePath;
	/**
	 * Wallpaper image type;
	 */
	private String imageType;
	/**
	 * Wallpaper width.
	 */
    private String width;
    /**
     * Wallpaper height.
     */
    private String height;
    /**
     * Wallpaper author.
     */
    private String author;
    /**
     * Author profile URL.
     */
    private String authorProfileURL;
    /**
     * Wallpaper provider.
     */
    private String provider;
    /**
     * Wallpaper provider URL.
     */
    private String providerURL;
    /**
     * Original URL where the wallpaper can be downloaded.
     */
    private String originalURL;
    
    /**
     * Getters and setters.
     */

	/**
	 * Gets absolutePath.
	 * @return the absolutePath
	 */
	public String getAbsolutePath() {
		return this.absolutePath;
	}

	/**
	 * Sets {@link #absolutePath}.
	 * @param absolutePath the absolutePath to set
	 */
	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	/**
	 * Gets imageType.
	 * @return the imageType
	 */
	public String getImageType() {
		return this.imageType;
	}

	/**
	 * Sets {@link #imageType}.
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	/**
	 * Gets width.
	 * @return the width
	 */
	public String getWidth() {
		return this.width;
	}

	/**
	 * Sets {@link #width}.
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}

	/**
	 * Gets height.
	 * @return the height
	 */
	public String getHeight() {
		return this.height;
	}

	/**
	 * Sets {@link #height}.
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * Gets author.
	 * @return the author
	 */
	public String getAuthor() {
		return this.author;
	}

	/**
	 * Sets {@link #author}.
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Gets authorProfileURL.
	 * @return the authorProfileURL
	 */
	public String getAuthorProfileURL() {
		return this.authorProfileURL;
	}

	/**
	 * Sets {@link #authorProfileURL}.
	 * @param authorProfileURL the authorProfileURL to set
	 */
	public void setAuthorProfileURL(String authorProfileURL) {
		this.authorProfileURL = authorProfileURL;
	}    
	
	/**
	 * Gets provide.
	 * @return the provider
	 */
	public String getProvider() {
		return this.provider;
	}

	/**
	 * Sets {@link #provide}.
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * Gets providerURL.
	 * @return the providerURL
	 */
	public String getProviderURL() {
		return this.providerURL;
	}

	/**
	 * Sets {@link #providerURL}.
	 * @param providerURL the providerURL to set
	 */
	public void setProviderURL(String providerURL) {
		this.providerURL = providerURL;
	}

	/**
	 * Gets originalURL.
	 * @return the originalURL
	 */
	public String getOriginalURL() {
		return this.originalURL;
	}

	/**
	 * Sets {@link #originalURL}.
	 * @param originalURL the originalURL to set
	 */
	public void setOriginalURL(String originalURL) {
		this.originalURL = originalURL;
	}

    /**
     * Constructor.
     * @param absolutePath Absolute path where the wallpaper is stored
     */
    public Wallpaper(String absolutePath)
    {
        this.absolutePath = absolutePath;
        this.width = NOT_AVAILABLE;
        this.height = NOT_AVAILABLE;
        this.author = NOT_AVAILABLE;
        this.authorProfileURL = NOT_AVAILABLE;
        this.provider = NOT_AVAILABLE;
        this.providerURL = NOT_AVAILABLE;
        this.originalURL = NOT_AVAILABLE;
        this.imageType = NOT_AVAILABLE;

        // Getting image type
        String absolutePathSplitted[] = absolutePath.split("\\.");
        String extension = absolutePathSplitted[absolutePathSplitted.length -1];
        switch (extension.toLowerCase()) {
		case WALLPAPER_IMAGE_TYPE_JPG:
			// JPG
			this.imageType = WALLPAPER_IMAGE_TYPE_JPG; 
			break;

		case WALLPAPER_IMAGE_TYPE_JPEG:
			// JPEG
			this.imageType = WALLPAPER_IMAGE_TYPE_JPEG; 
			break;

		case WALLPAPER_IMAGE_TYPE_PNG:
			// PNG
			this.imageType = WALLPAPER_IMAGE_TYPE_PNG; 
			break;

		default:
			this.imageType = WALLPAPER_IMAGE_TYPE_UNKNOWN; 
			break;
		}

        // Getting provider and landing URL
        PropertiesManager propertiesManager = PropertiesManager.getInstance();
        
        if (absolutePath.contains(Provider.BING_PREFIX)) {
        	this.provider = Provider.BING_PROVIDER;
        	this.providerURL = propertiesManager.getProperty("provider.bing.landing.url");
        } else if (absolutePath.contains(Provider.DEVIANT_ART_PREFIX)) {
        	this.provider = Provider.DEVIANT_ART_PROVIDER;
        	this.providerURL = propertiesManager.getProperty("provider.deviantart.landing.url");
        } else if (absolutePath.contains(Provider.DUAL_MONITOR_BACKGROUNDS_PREFIX)) {
        	this.provider = Provider.DUAL_MONITOR_BACKGROUNDS_PROVIDER;
        	this.providerURL = propertiesManager.getProperty("provider.dualMonitorBackgrounds.landing.url");
        } else if (absolutePath.contains(Provider.SOCIAL_WALLPAPERING_PREFIX)) {
        	this.provider = Provider.SOCIAL_WALLPAPERING_PROVIDER;
        	this.providerURL = propertiesManager.getProperty("provider.socialWallpapering.landing.url");
        } else if (absolutePath.contains(Provider.UNSPLASH_PREFIX)) {
        	this.provider = Provider.UNSPLASH_PROVIDER;
        	this.providerURL = propertiesManager.getProperty("provider.unsplash.landing.url");
        } else if (absolutePath.contains(Provider.WALLHAVEN_PREFIX)) {
        	this.provider = Provider.WALLHAVEN_PROVIDER;
        	this.providerURL = propertiesManager.getProperty("provider.wallhaven.landing.url");
        } else if (absolutePath.contains(Provider.WALLPAPER_FUSION_PREFIX)) {
        	this.provider = Provider.WALLPAPER_FUSION_PROVIDER;
        	this.providerURL = propertiesManager.getProperty("provider.wallpaperFusion.landing.url");
        }
        
        // Getting resolution
        File wallpaperFile = new File(absolutePath);
        BufferedImage wallpaperImage;
        int wallpaperImageHeight = 0;
        int wallpaperImageWidth = 0;
		try {
			wallpaperImage = ImageIO.read(wallpaperFile);
	        wallpaperImageHeight = wallpaperImage.getHeight();
	        wallpaperImageWidth = wallpaperImage.getWidth();
	        
			if (wallpaperImageWidth > 0) {
				this.width = String.valueOf(wallpaperImageWidth);
			}

			if (wallpaperImageHeight > 0) {
				this.height = String.valueOf(wallpaperImageHeight);
			}

		} catch (IOException exception) {
			LOG.error("The wallpaper " + absolutePath + " couldn't be read to obtain its resolution. Error: " + exception.getMessage());
		}
		
		// Getting metadata information if wallpaper is JPG or JPEG
		if (this.imageType.equals(WALLPAPER_IMAGE_TYPE_JPEG) || this.imageType.equals(WALLPAPER_IMAGE_TYPE_JPG)) {
	        ImageMetadata metadata;
			try {
				metadata = Imaging.getMetadata(wallpaperFile);
		        if (metadata instanceof JpegImageMetadata) {
		            final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

		            // Jpeg EXIF metadata is stored in a TIFF-based directory structure
		            // and is identified with TIFF tags.
		            TiffField field;
		            // Author
		            field = jpegMetadata.findEXIFValueWithExactMatch(MicrosoftTagConstants.EXIF_TAG_XPAUTHOR);
		            if (field != null) {
		            	this.author = field.getValueDescription().trim().replace("'", ""); 
		            }
		            
		            // Author profile URL
		            field = jpegMetadata.findEXIFValueWithExactMatch(ExifTagConstants.EXIF_TAG_SOFTWARE);
		            if (field != null) {
		            	this.authorProfileURL = field.getValueDescription().trim().replace("'", ""); 
		            }
		        }
			} catch (ImageReadException | IOException exception) {
				LOG.error("Metadata related to the wallpaper " + absolutePath + " couldn't be read. Error: " + exception.getMessage());
			}
		}
    }
}
